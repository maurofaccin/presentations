
SRCS = $(shell ls -d */)

.PHONY: $SRC


all: $(SRCS)
	@echo $(SRCS)

$(SRCS): .FORCE
	$(MAKE) -C $@

.FORCE:

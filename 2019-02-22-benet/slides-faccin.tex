% TEX program = lualatex
\RequirePackage{luatex85}
\documentclass[aspectratio=169]{beamer}
\usepackage[english]{babel}
\usepackage{graphicx}
\usepackage{enumitem}
\setlist[itemize]{leftmargin=0pt,
    rightmargin=0pt,
    label=\textbullet}

\usepackage{booktabs}

\usepackage{tikz}
\usetikzlibrary{calc, matrix, positioning, arrows, arrows.meta}
\usepackage{pgfplots}
\usepgfplotslibrary{groupplots}
\usetikzlibrary{patterns}

\newcommand{\solidsquare}[3]{%
  \fill[#3] (#1, #2) rectangle (#1 + 1.0, #2 + 1.0);
}
\newcommand{\linessquare}[4]{%
  \fill[%
    pattern=#4,
    % pattern=crosshatch dots,
    %pattern=horizontal lines,
    pattern color=#3] (#1, #2) rectangle (#1 + 1.0, #2 + 1.0);
}

\usepackage{amsmath,amsthm, amssymb, latexsym}
\DeclareMathOperator{\cov}{Cov}
\boldmath%

\usepackage{pgfplots}
\usepgfplotslibrary{dateplot}

\usetheme[progressbar=frametitle,
          sectionpage=none,
          background=dark,
          block=fill]{metropolis}
\usepackage{etoolbox}
% \usecolortheme[snowy]{owl}
% \usetheme{mod}

\makeatletter
% \setlength{\metropolis@titleseparator@linewidth}{1pt}
% \setlength{\metropolis@progressonsectionpage@linewidth}{2pt}
\setlength{\metropolis@progressinheadfoot@linewidth}{1pt}
\makeatother

\title{Variable aggregation for dynamical systems on networks.}

\subtitle{}
\author{\itshape Mauro Faccin}
\date{\normalsize Benet 2019 (Hasselt University)}
\setbeamertemplate{frame footer}{M.\,Faccin, Hasselt 2019}
% \titlegraphic{%
%   \tikz[remember picture, overlay]%
%     \node[opacity=.1, anchor=center]%
%       at (current page.center){%
%         \includegraphics[width=1.5\textwidth]{./pics/Franklins_chart_of_the_Gulf_Stream.jpg}
%       };
% }

\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}

\definecolor{mplRED}{RGB}{226, 74,  51}
\definecolor{mplBLU}{RGB}{52,  138, 189}
\definecolor{mplPUR}{RGB}{152, 142, 213}

\definecolor{C0}{RGB}{27,158,119}
\definecolor{C1}{RGB}{217,95,2}
\definecolor{C2}{RGB}{117,112,179}
% \definecolor{C0}{HTML}{66c2a5}
% \definecolor{C1}{HTML}{fc8d62}
% \definecolor{C2}{HTML}{8da0cb}
\definecolor{C3}{HTML}{e78ac3}
\definecolor{C4}{HTML}{a6d854}
\definecolor{C5}{HTML}{ffd92f}
\definecolor{C4}{HTML}{9467bd}
\definecolor{Cg}{HTML}{7f7f7f}

\setbeamercolor{bred}{fg=, bg=mplRED}
\setbeamercolor{bblu}{fg=, bg=mplBLU}
\setbeamercolor{bpur}{fg=, bg=mplPUR}

% \addtobeamertemplate{frametitle}{}{%
%   \begin{tikzpicture}[overlay, remember picture]
%     \node[anchor=north east] at (current page.north east)
%       {\includegraphics[width=2cm]{./pics/clustered.pdf}};
%   \end{tikzpicture}
% }

\newcommand\boxedtext[1]{%
  \begin{tikzpicture}
    \usebeamercolor{block title}
    \node[fg, fill=bg, inner sep=3pt] (txt) {
        \begin{minipage}{0.9\linewidth}
          #1 
        \end{minipage}
      };
    \draw[line width=2pt, black!50] (txt.north west) -- (txt.south west);
  \end{tikzpicture}
}

\newcommand\standouttext[1]{%
  \begin{center}
    \usebeamercolor{block title}
    \tikz{\node[fill=bg, text=fg, inner sep=3mm, font=\Large] {#1};}
  \end{center}
}

\newcommand\reference[1]{%
  \let\thefootnote\relax\footnotetext{\tiny #1}
}

\newcommand\crule[3]{%
  \textcolor{#1}{\rule[-1mm]{#2}{#3}}
}
\def\F{\mathcal F}

\newcommand{\blocks}[1]{%
  \begin{center}
  \begin{tikzpicture}
      \let\mymatrixcontent\empty
      \foreach \row in #1 {%
        \foreach \col in \row{%
          \gappto\mymatrixcontent{|[}
          \expandafter\gappto\expandafter\mymatrixcontent\expandafter{\col}%
          \gappto\mymatrixcontent{]|\&}
        }
        \gappto\mymatrixcontent{\\}%
      }
      \matrix [matrix of nodes, draw,
          nodes={minimum size=0.5cm},
          w/.style={},
          d/.style={fill=black!60},
          l/.style={fill=black!30},
          ampersand replacement=\&] (m) {%
          \mymatrixcontent
      };

      \draw[very thick] ($ (m.north)+(0,0.2cm) $) -- ($ (m.south)-(0,0.2cm) $);
      \draw[very thick] ($ (m.east)+(0.2cm,0) $) -- ($ (m.west)-(0.2cm,0) $);
  \end{tikzpicture}
  \end{center}
}

\newenvironment{coloredblock}[3]{%
  \setbeamercolor{block title}{#1}
  \setbeamercolor{block body}{#2}
  \begin{block}{#3}}{\end{block}}

\begin{document}

\begin{frame}[plain] %Title
  \maketitle
\end{frame}

\begin{frame}
  \frametitle{Projected Markov Chain}
  \begin{columns}[t]
    \begin{column}{0.5\textwidth}
      \begin{block}{Markov Chain}
        $$\ldots, x_\text{past}, x_\text{now}, x_\text{future}, \ldots$$
      \end{block}
      \begin{center}
        \includegraphics[width=0.7\textwidth]{./pics/clustering.pdf}
      \end{center}
    \end{column}
    \begin{column}{0.5\textwidth}
      \begin{block}{Projection}
        $$\ldots, Y_\text{past}, Y_\text{now}, Y_\text{future}, \ldots$$
      \end{block}
      \begin{center}
        \includegraphics[width=0.7\textwidth]{./pics/clustered.pdf}
      \end{center}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Complexity}
  \standouttext{Where did the complexity go?}
  \begin{columns}
    \column{0.5\textwidth}
      \usebeamercolor{block title}
      \begin{tikzpicture}[
        scale=1, transform shape,
        vert/.style={
          fill=C0,
          draw=none,
          circle,
        },
        ]
        \node[vert, fill=C2, inner sep=5mm] (A) {};
        \node[vert, below left=of A, inner sep=3mm] (B) {};
        \node[vert, below right=of A, fill=C1, inner sep=4mm] (C) {};
        
        \draw[fg, line width=1mm] (B) -- (A) -- (C);
      \end{tikzpicture}
    
    \column{0.5\textwidth}
    \pause%
    \boxedtext{ Part of the complexity is now hidden in the [projected] dynamics. }
    \boxedtext{ Emergence of \textbf{effective memories}. }
    
  \end{columns}
\end{frame}

\begin{frame}
  \frametitle{Effective Memory}

  How can we compute the emerfing memory effects?

  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[%
          width =0.7\textwidth,
          height=0.5\paperheight,
          bar width=18pt,
          clip=false,
          hide axis
        ]
        \addplot [%
          ybar,
          draw=C1,
          fill=C1,
          ] coordinates {(0,  40) (10, 22) (20, 16) (30, 12) (40, 11)};
        \addplot[mark=none] coordinates {(-10, 10) (50, 10)};
        \node[anchor=south] at (axis cs:0, 40) {$H_0=H(Y_t)$};
        \node[anchor=south west] at (axis cs:5, 22) {$H_1=H(Y_t|Y_{t-1})$};
        \node[anchor=south west] at (axis cs:15, 16) {$H_2=H(Y_t | Y_{t-1}, Y_{t-2})$};
        \node[anchor=south] at (axis cs:30, 11) {$H_3$};
        \node[anchor=south] at (axis cs:40, 10) {$H_4$};
        \node[anchor=west] at (axis cs:50, 10) {$H_r$};
    \end{axis}
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Eigen Partitions}

    \usebeamercolor{block title}
    \begin{center}
      \begin{tikzpicture}[
        vert/.style={draw, circle, fg},
        scale=0.5,
        ]

        \begin{scope}
          \node[vert] at (0, 0) (1) {};
          \node[vert] at (1, 2) (2) {};
          \node[vert] at (2, 1) (3) {};
          \node[vert] at (4, 1) (4) {};
          \node[vert] at (5, 0) (5) {};
          \node[vert] at (6, 2) (6) {};

          \draw[thick] (1) -- (2) -- (3) -- (1);
          \draw[thick] (4) -- (5) -- (6) -- (4);
          \draw[thick] (3) -- (4);
        \end{scope}

        \begin{scope}[yshift=3cm, xshift=8cm, yscale=2]
          \foreach \eig in {0,-0.205,-1.16, -1.5, -1.63}
            \draw [very thick] (0, \eig) -- (1, \eig);
          \node[anchor=center] at (0.5, 0.5) {Eigenvalues};

          \only<2>{\draw[very thick, C1] (0, 0) -- (1, 0);}
          \only<3>{\draw[very thick, C1] (0, -0.205) -- (1, -0.205);}
          \only<4>{\draw[very thick, C1] (0, -1.16) -- (1, -1.16);}
        \end{scope}

        \only<2>{\draw[very thick, C1] (0, 3) -- (6, 3);}
        \only<3>{\draw[very thick, C1] (0, 4) -- (1, 4) -- (2, 3.8) -- (4, 3.2) -- (5, 3) -- (6, 3);}
        \only<4>{\draw[very thick, C1] (0, 3) -- (1, 3) -- (2, 4) -- (4, 4) -- (5, 3) -- (6, 3);}

        \only<3>{%
            \node[vert, fill=C1] at (1) {};
            \node[vert, fill=C1] at (2) {};
            \node[vert, fill=C1!80!bg] at (3) {};
            \node[vert, fill=C1!20!bg] at (4) {};
        }

        \only<4>{%
            \node[vert, fill=C1] at (3) {};
            \node[vert, fill=C1] at (4) {};
        }

      \end{tikzpicture}
    \end{center}
\end{frame}

\begin{frame}{Bow tie}
    \usebeamercolor{block title}
    \begin{tikzpicture}[
      ampersand replacement=\&,
      vert/.style={circle, draw, minimum size=5mm}
      ]
      \matrix (mat1) [matrix of nodes,
              column sep=1cm,
              a/.style={fg, fill=C0},
              b/.style={fg, fill=none},
              column sep=2mm,
              nodes={vert},
              ]{%
                |[a]| {} \&\&\&\& |[b]| {}\\
                \& |[a]| {} \&\& |[b]| {}\\
                |[a]| {} \&\&\&\& |[b]| {}\\
      };
      \draw[fg, very thick]
      (mat1-2-2) -- (mat1-1-1) -- (mat1-3-1) -- (mat1-2-2)
      -- (mat1-2-4) -- (mat1-1-5) -- (mat1-3-5) -- (mat1-2-4);

      \matrix (mat2) [matrix of nodes,
              below=of mat1,
              column sep=1cm,
              a/.style={fg, fill=none},
              b/.style={fg, fill=C1},
              column sep=2mm,
              nodes={vert},
              ]{%
                |[a]| {} \&\&\&\& |[a]| {}\\
                \& |[b]| {} \&\& |[b]| {}\\
                |[a]| {} \&\&\&\& |[a]| {}\\
      };
      \draw[fg, very thick]
      (mat2-2-2) -- (mat2-1-1) -- (mat2-3-1) -- (mat2-2-2)
      -- (mat2-2-4) -- (mat2-1-5) -- (mat2-3-5) -- (mat2-2-4);

       \begin{semilogyaxis}[%
         at = {($(mat2.south east) + (10mm, 8mm)$)},
           width = 10cm,
           height = 5cm,
           ybar = 0, % zero sep between grouped bars
           ymin = 1e-3, ymax = 1,
           xmin = -0.5, xmax = 3.5,
           ytick = {1e-3, 1e-2, 1e-1, 1},
           xtick = {0, 1, 2, 3, 4},
           xticklabels = {$I_0$,$I_1$,$I_2$,$I_3$,$I_4$},
           x tick style = transparent,
           y tick style = transparent,
           axis x line*=bottom,
           axis y line*=left,
           % ymajorgrids = true,
           bar width=20pt,
           log origin y=infty,
           axis line style = {thick, draw=black!20},
           major grid style = {thick, draw=black!20},
           legend style={draw=none,
               at={(0.99, 0.99)},
               cells={align=left},
               fill=none,
             },
           legend cell align={left}
         ]
         \addplot [%
           draw opacity=0,
           fill=C0,
           area legend,
           ] table [row sep=crcr] {%
             x y\\
             0 0.4558\\
             1 0.0475\\
             2 0.0184\\
             3 0.00116\\
           };
         \addplot [%
           draw opacity=0,
           fill=C1,
           area legend,
           ] table [row sep=crcr]{%
             x y \\
             0  2.02e-2\\
           };
         \legend{{Assortative\\$h_\text{KS}=0.644$},
                 {Equitable\\$h_\text{KS}=0.965$}};
     \end{semilogyaxis}
     \onslide<1->
    \end{tikzpicture}
\end{frame}

\begin{frame}[standout]
  \thispagestyle{empty}

  Partitioning with dynamics

\end{frame}

\begin{frame}{Random walk covariance}
  \begin{columns}
    \column{0.5\linewidth}
    A walker is visiting nodes on the network.
    Let's measure how it get trapped in a partition.

    Let's define:
    \begin{description}
      \item[$\chi_c$] characteristic function of partition $c$
    \end{description}
    
    \column{0.5\linewidth}
    Covariance of partitions along the dynamics:

    \begin{align*}
      \cov(X, Y) &= E(XY) - E(X)E(Y)\\
      E(\chi_c(t) \chi_c(t-1)) &= \frac 1{2m} \sum_{ij\in c} A_{ij}\\
      E(\chi_c(t)) &= \frac 1{2m} \sum_{i\in c} k_i
    \end{align*}

  \end{columns}
\end{frame}

\begin{frame}{Modularity}
  \begin{columns}
    \column{0.5\linewidth}
    Modularity:
    \begin{equation*}
      Q = \frac 1{2m}
        \sum_{ij} \left[ A_{ij} - \frac {k_i k_j}{2m}\right]\delta(c_i, c_j)
    \end{equation*}
    
    \column{0.5\linewidth}
    \begin{block}{Random walker covariance}
    $\chi_c$ characteristic function of partition $c$
    \begin{equation*}
      Q\propto \sum_c \cov\left(\chi_c(t), \chi_c(t+1)\right) 
    \end{equation*}
    \end{block}

  \end{columns}
  \vfill
  Linear correlation between consecutive time-steps.

  \reference{Shen et al. (2010) PRE, 82, 016114}
    
\end{frame}

\begin{frame}
  \frametitle{Back to the Entrogram}
  \begin{center}
    \begin{tikzpicture}
      \begin{axis}[%
          width =0.7\textwidth,
          height=0.5\paperheight,
          bar width=18pt,
          clip=false,
          hide axis
        ]
        \addplot [%
          ybar,
          draw=C0,
          fill=C0,
          ] coordinates {(10, 12) (20, 6) (30, 2) (40, 1)};
        \addplot [%
          ybar,
          draw=C1,
          fill=C1,
          ] coordinates {(0,  30)};
        \addplot[mark=none] coordinates {(-10, 0) (50, 0)};
        \node[anchor=south] at (axis cs:0, 30) {$I_0=I(Y_t;Y_{t-\tau},\ldots)$};
        \node[anchor=south west] at (axis cs:5, 12)
          {$I_1=I(Y_t;Y_{t-2\tau},\ldots|Y_{t-\tau})$};
        \node[anchor=south] at (axis cs:20, 6) {$I_2$};
        \node[anchor=south] at (axis cs:30, 2) {$I_3$};
        \node[anchor=south] at (axis cs:40, 1) {$I_4$};
        \node[anchor=west] at (axis cs:50, 0) {$H_r$};

        \addplot[mark=none] coordinates {(6, 12) (-10, 12) (-10, 30) (-4, 30)};
        \node[anchor=east] at (axis cs:-10, 21) {$I(Y_t; Y_{t-\tau})$};
        \node at (axis cs:-40, 21) {};
    \end{axis}
    \end{tikzpicture}
  \end{center}
  where $\tau$ represents a time-scale parameter.
  \reference{M.F. et al, Journal of Complex Networks, cnx055}
\end{frame} 

\begin{frame}{What about generative models?}
  \begin{columns}
    \begin{column}{0.6\textwidth}
      \begin{align*}
        I(Y_t; Y_{t-1}) &= H(Y_y) + H(Y_{t-1}) - H(Y_y, Y_{t-1})\\
        H(Y_t) &= - \sum_c \frac{e_c}{2m} \log \frac{e_c}{2m} \qquad e_c = \sum_{i\in c, j} A_{ij}\\
        H(Y_t, Y_{y-1}) &= - \sum_{cd} \frac{e_{cd}}{2m} \log \frac{e_{cd}}{2m} \qquad e_{cd} = \sum_{i\in c, j\in d} A_{ij}
      \end{align*}
    \end{column}
    \begin{column}{0.4\textwidth}
      \pause
      \begin{block}{DC-SBM}
        \begin{equation*}
          \mathcal{S}_C \propto -\frac 12 \sum_{cd} e_{cd} \log \frac{e_{cd}}{e_c e_d}
        \end{equation*}        
      \end{block}
    \end{column}
  \end{columns}

  \reference{In binary undirected networks}
\end{frame}

\begin{frame}{Non linear communities}

  \begin{center}
  \begin{tikzpicture}[scale=1, transform shape]
  
    \usebeamercolor{block title}
    \node[font=\large, bg, fill=fg, inner sep=3mm] (obj) {%
      Objective function: $I(Y_t, Y_{t-\tau})$};
    
    \node[below left=of obj.south] (mod) {%
        \parbox{5cm}{%
          \footnotesize\center%
          Modularity\\
          $Q\propto \sum_c \cov\left(\chi_c(t), \chi_c(t+1)\right) $
        }
      };
    \node[below right=2cm of obj.south] (sbm) {%
        \parbox{5cm}{%
          \footnotesize\center%
          DC-SBM\\
          $I(Y_t; Y_{t-\tau}) \propto - \sum_{rs} e_{rs} \log \frac{e_{rs}}{e_r e_s}$\\
          \scriptsize In some cases
        }
      };
    \node[below=2cm of obj.south] (infomap) {%
        \parbox{5cm}{%
          \footnotesize\center%
          Spectral Properties
        }
      };
    \draw[-Bar, black!20, very thick, shorten <= 2mm] (obj) -- (mod);
    \draw[Latex-Latex, black!20, very thick, shorten <= 2mm] (obj) -- (sbm);
    \draw[-Bar, black!20, very thick, shorten <= 2mm] (obj) -- (infomap);
  \end{tikzpicture}
  \end{center}
  
  \reference{Shen et al. (2010) PRE, 82, 016114.}
  \reference{Karrer and Newman (2011), PRE 83, 016107.}
  \reference{Rosvall and Bergstrom (2008) PNAS 105, 1118.}
\end{frame}

\begin{frame}[standout]
  \thispagestyle{empty}

  Where the dynamical part enter?

\end{frame}

\begin{frame}
  \frametitle{Example 1: One cycle}
      How many partitions?

      \begin{center}
      \usebeamercolor{block title}
        \begin{tikzpicture}
          [
          link/.style={fg!50!bg},
          vert/.style={circle, very thin, draw=fg, fill=fg, minimum size=3mm, inner sep=0},
          ]
              \begin{scope}[%
                local bounding box=net11,
                ]
                \foreach \n in {0,30,...,330}{
                  \draw[link] (\n:10mm) -- (\n+30:10mm);
                  \draw[link] (\n:10mm) -- (\n+60:10mm);
                  \draw[link] (\n:10mm) -- (\n+90:10mm);
                }
                \foreach \n in {1,2,3}%
                   \node[vert, fill=C1] at (\n*30: 10mm) (A\n) {};
                \foreach \n in {4,5,6}%
                   \node[vert, fill=C2] at (\n*30: 10mm) (A\n) {};
                \foreach \n in {7,8,9}%
                   \node[vert, fill=C3] at (\n*30: 10mm) (A\n) {};
                \foreach \n in {10,11,12}%
                   \node[vert, fill=C0] at (\n*30: 10mm) (A\n) {};
              \end{scope}

              \begin{scope}[
                xshift=-3cm,
                ]
                \draw[fg] (-1,-1) rectangle (1, 1);
                \draw[draw=none, fill=fg] (1, 1) -- (0.8, 1) -- (1, 0.8);
                \draw[draw=none, fill=fg] (-1, -1) -- (-0.8, -1) -- (-1, -0.8);
                \draw[draw=none, fill=fg] (-1, 1) -- (-1, 0.8)
                  -- (0.8, -1) -- (1, -1) -- (1, -0.8) -- (-0.8, 1);

                \only<2>{%
                \foreach \n in {-1,-0.6,...,0.8} {%
                  \draw[red!50!fg, fill=red!10!fg, fill opacity=0.5] ($(\n, -1 * \n)$) rectangle ($(\n + 0.4, -1 * \n - 0.4)$);
                }
                }
                
		\node at (-1.5, 0) {Adj:};
              \end{scope}

              \pause
              \pause
              \begin{scope}[%
                local bounding box=net12,
                xshift=3cm,
                ]
                \foreach \n in {0,30,...,330}{
                  \draw[link] (\n:10mm) -- (\n+30:10mm);
                  \draw[link] (\n:10mm) -- (\n+60:10mm);
                  \draw[link] (\n:10mm) -- (\n+90:10mm);
                }
                \foreach \n in {1,...,12}%
                   \node[vert, fill=C0] at (\n*30: 10mm) (A\n) {};
              \end{scope}
              \draw[->, very thick, fg] (-12mm, -12mm) -- (42mm, -12mm)
                node[yshift=-3mm] {$\tau$};
          \onslide<1->
        \end{tikzpicture}

      \end{center}
\end{frame}

\begin{frame}
  \frametitle{Example 3. Ocean buoys}
  \begin{tikzpicture}[remember picture, overlay]
    \node[right=of current page.west] {\includegraphics[width=4cm]{pics/drifter-pic.jpg}};
    \node at (current page.center) {\includegraphics[width=5cm]{pics/drifter-deploying.jpg}};
    \node[left= of current page.east] {\includegraphics[width=4cm]{pics/drifter-schematic.jpg}};
    % \node[anchor=center] at (current page.center)
    %   {\includegraphics[width=10cm]{pics/drifters-north-atlantic-ocean.jpg}};
  \end{tikzpicture}
\end{frame}

\begin{frame}[c]
  \frametitle{Example 3. Ocean buoys}
  \begin{columns}
    \column{.5\textwidth}
    \centering
    $\tau=7$ days
    \\ \vspace{3mm}
    \includegraphics[width=\linewidth]{pics/buoy-7.png}
    \centering
    \pause%
    $\tau=700$ days
    \\ \vspace{3mm}
    \includegraphics[width=\linewidth]{pics/buoy-700.png}

    \column{.5\textwidth}
    \pause%
    \centering
      \includegraphics[width=\linewidth]{pics/Currents-brit-encicl.png}
  \end{columns}
\end{frame}

\begin{frame}[c]
  \thispagestyle{empty}
  \begin{tikzpicture}[
    scale=1, transform shape,
    remember picture, overlay,
    ]
    \node[anchor=center] at (current page.center) (map)
      {\includegraphics[width=25cm]{pics/buoy-700.png}};

    \node[anchor=north west] at (current page.north west)
      {\includegraphics[width=0.5\linewidth]{pics/Currents-brit-encicl.png}};

  \end{tikzpicture}
\end{frame}

\begin{frame}
  \frametitle{Questions?}
  \begin{columns}[c]
  \column{0.5\textwidth}

    \includegraphics[width=\linewidth]{pics/Franklins_chart_of_the_Gulf_Stream.jpg}

  \medskip
  \column{0.5\textwidth}
    \begin{block}{Joint work with:}
      JC Delvenne
      \raisebox{-2.5mm}{
      \includegraphics[height=5mm]{pics/logo-icteam.pdf}}\\
      M Schaub
      \raisebox{-2.5mm}{
      \includegraphics[height=5mm]{pics/idss.png}
    % \includegraphics[height=5mm]{pics/oxford.png}
    }
    \end{block}
    \bigskip%
    \centering
    \scriptsize{\url{https://maurofaccin.github.io}}\\
    \scriptsize{\url{mauro.faccin@uclouvain.be}}\\
    \bigskip%
    Code at:\\
    \url{https://github.com/maurofaccin/entropart}

  \end{columns}
\end{frame}
\end{document}
